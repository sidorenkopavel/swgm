@extends('layouts.app')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('dropzone/dist/min/dropzone.min.css')}}">

<!-- JS -->
<script src="{{asset('dropzone/dist/min/dropzone.min.js')}}" type="text/javascript"></script>

@section('content')
    <div class="container">
        <h1>ФИО</h1>
        <p>Пара</p>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <!-- Dropzone -->
                    <form action="{{route('personal_photos')}}" class='dropzone' >
                        {{ csrf_field() }}
                    </form>

            </div>
            <div class="col-md-12 col-md-offset-2">
                <h2>Фотоальбомы</h2>
            </div>
            <div class="col-md-12 col-md-offset-2">
                <h2>Интересы</h2>
                <form method="post" action="{{route('personal')}}">
                    {{ csrf_field() }}
                @foreach($arResult['interestings'] as $arInteresting)
                    <label class="col-sm-6">
                        {{$arInteresting}}
                        <input @if(isset($arResult['params']['data']['interestings']))
                               @if(in_array($arInteresting,$arResult['params']['data']['interestings'])) checked @endif
                               @endif
                               type="checkbox" value="{{$arInteresting}}" class="tqInterestings" name="personal[interestings][]">
                    </label>
                @endforeach
                </form>
            </div>
            <div class="col-md-12 col-md-offset-2">
                <h2>Айда в отпуск</h2>



                @foreach($arResult['vacations'] as $arVacation)
                <div class="row">
                    <form method="post" action="{{route('personal')}}">
                        {{ csrf_field() }}
                        <input required type="date" value="{{$arVacation['params']['date_from'] or ''}}"
                               placeholder="c {{date('d.m.Y')}}" name="vacations[{{$arVacation['id']}}][date_from]">
                        <input required type="date" value="{{$arVacation['params']['date_to'] or ''}}"
                               placeholder="до {{date('d.m.Y')}}" name="vacations[{{$arVacation['id']}}][date_to]">
                        <input required type="text" value="{{$arVacation['params']['city'] or ''}}"
                               placeholder="Город" name="vacations[{{$arVacation['id']}}][city]">
                        <input type="submit" value="Обновить">
                        <a href="{{route('remove_vacation',$arVacation['id'])}}">Удалить</a>
                    </form>
                </div>
                @endforeach
                <div class="row">
                    <form method="post" action="{{route('personal')}}">
                        {{ csrf_field() }}
                        <input required type="date" placeholder="c {{date('d.m.Y')}}" name="vacations[new][date_from]">
                        <input required type="date" placeholder="до {{date('d.m.Y')}}" name="vacations[new][date_to]">
                        <input required type="text" placeholder="Город" name="vacations[new][city]">
                        <input type="submit" value="Добавить">
                    </form>
                </div>
            </div>
            <div class="col-md-12 col-md-offset-2">
                <h2>Личная информация</h2>
                <form method="post" action="{{route('personal')}}">
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <textarea
                                name="personal[information]">{{$arResult['params']['data']['information'] or ''}}</textarea>
                    </div>
                    <?if(strtolower($arResult['params']['who']) == 'пара'){?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Он</th>
                            <th scope="col">Она</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Дата рождения</th>
                            <td><input value="{{$arResult['params']['data']['he']['age'] or ''}}" type="date"
                                       name="personal[he][age]"></td>
                            <td><input value="{{$arResult['params']['data']['she']['age'] or ''}}" type="date"
                                       name="personal[she][age]"></td>
                        </tr>
                        <tr>
                            <th scope="row">Рост (в см.)*</th>
                            <td><input value="{{$arResult['params']['data']['he']['growth'] or ''}}"
                                       name="personal[he][growth]" type="text"></td>
                            <td><input value="{{$arResult['params']['data']['she']['growth'] or ''}}"
                                       name="personal[she][growth]" type="text"></td>
                        </tr>
                        <tr>
                            <th scope="row">Вес (в кг.)*</th>
                            <td><input value="{{$arResult['params']['data']['he']['weight'] or ''}}"
                                       name="personal[he][weight]" type="text"></td>
                            <td><input value="{{$arResult['params']['data']['she']['weight'] or ''}}"
                                       name="personal[she][weight]" type="text"></td>
                        </tr>
                        <tr>
                            <th scope="row">Телосложение</th>
                            <td><input value="{{$arResult['params']['data']['he']['body_type'] or ''}}"
                                       name="personal[he][body_type]" type="text"></td>
                            <td><input value="{{$arResult['params']['data']['she']['body_type'] or ''}}"
                                       name="personal[she][body_type]" type="text"></td>
                        </tr>
                        <tr>
                            <th scope="row">Происхождение</th>
                            <td><input value="{{$arResult['params']['data']['he']['age'] or ''}}"
                                       name="personal[he][ethical]" type="text"></td>
                            <td><input value="{{$arResult['params']['data']['she']['ethical'] or ''}}"
                                       name="personal[she][ethical]" type="text"></td>
                        </tr>
                        </tbody>
                    </table>
                    <?} else {?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Дата рождения</th>
                            <td><input value="{{$arResult['params']['data']['me']['age'] or ''}}" type="date"
                                       name="personal[me][age]"></td>
                        </tr>
                        <tr>
                            <th scope="row">Рост (в см.)*</th>
                            <td><input value="{{$arResult['params']['data']['me']['growth'] or ''}}"
                                       name="personal[me][growth]" type="text"></td>
                        </tr>
                        <tr>
                            <th scope="row">Вес (в кг.)*</th>
                            <td><input value="{{$arResult['params']['data']['me']['weight'] or ''}}"
                                       name="personal[me][weight]" type="text"></td>
                        </tr>
                        <tr>
                            <th scope="row">Телосложение</th>
                            <td><input value="{{$arResult['params']['data']['me']['body_type'] or ''}}"
                                       name="personal[me][body_type]" type="text"></td>
                        </tr>
                        <tr>
                            <th scope="row">Происхождение</th>
                            <td><input value="{{$arResult['params']['data']['me']['ethical'] or ''}}"
                                       name="personal[me][ethical]" type="text"></td>
                        </tr>
                        </tbody>
                    </table>
                    <?}?>
                    <input type="submit" name="submit" value="Сохранить">
                </form>
            </div>
        </div>
    </div>
@endsection
<script>
    $(document).on('change','.tqInterestings',function () {
        var form = $(this).parents('form');
        $.ajax({
            url: form.attr('action'),
            type: "POST",
            data: form.serialize(),
            success: function (result) {

            },
        });
    });
</script>
<style>
    #filtered-slick{
        height: 200px;
    }
</style>


@section('js_gallary')

    <script>

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        /*
        *
        * let mockFile = { name: "Filename", size: 12345 };
myDropzone.displayExistingFile(mockFile, 'https://image-url');
        * */

        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone", {
            maxFilesize: 10,
            maxFiles:10,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            removedfile: function(file) {
                var id = $(file._removeLink).attr('data-id');

                console.log(id);
                if(!id)
                {
                    id = window.users.users[file.name];

                }

                var fileName = file.name;
                var formData = new FormData();
                formData.append("id", id);
                formData.append("_token", CSRF_TOKEN);
                formData.append("file", fileName);
                $.ajax({
                    type: 'POST',
                    url: '{{route("remove_photos")}}',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    sucess: function(data){

                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function ($element,$result) {
                var elem = $($element)[0].previewElement;
                console.log($(elem).find('.dz-remove'));
                $(elem).find('.dz-remove').attr('data-id',$result.id);

                

            },
        });



        @if(isset($arResult['files']) && !empty($arResult['files']))

            @foreach($arResult['files'] as $file)
                var mockFile = { name: "{{$file['name']}}", size: '{{$file['size']}}' };
                myDropzone.displayExistingFile(mockFile, "{{$file['url']}}");
            @endforeach

            window.users = {
                    users: <?php echo json_encode($arResult['ITEM_IMAGE']) ?>
            }

        @endif


        myDropzone.on("sending", function(file, xhr, formData) {
            formData.append("_token", CSRF_TOKEN);
        });
    </script>

@endsection