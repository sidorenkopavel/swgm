@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Регистрация</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Кто вы</label>
                                <div class="error">{!! session()->get('error') !!}</div>
                                <div class="col-md-6">
                                    @foreach($arResult['WHO'] as $who)
                                        <label>
                                            <input type="radio"
                                                   name="who"
                                                   value="{{$who}}"
                                                   @if(isset($arResult['AUTH_DATA']['who']))
                                                   @if($arResult['AUTH_DATA']['who'] == $who)
                                                   checked
                                                   @endif
                                                   @endif
                                                   required
                                                   autofocus>
                                            {{$who}}
                                        </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Продолжить
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="step" value="who">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
