@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Регистрация</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="error">{!! session()->get('error') !!}</div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Он</label>
                                <div class="col-md-6">
                                   <input type="text" class="form-control" placeholder="Возраст" name="he[age]"
                                   value="{{$arResult['AUTH_DATA']['data']['he']['age'] or ''}}">
                                   <input type="text" class="form-control" placeholder="Рост" name="he[growth]"
                                          value="{{$arResult['AUTH_DATA']['data']['he']['growth'] or ''}}">
                                   <input type="text" class="form-control" placeholder="Телосложение" name="he[body_type]"
                                          value="{{$arResult['AUTH_DATA']['data']['he']['body_type'] or ''}}">
                                   <input type="text" class="form-control" placeholder="Этичная принадлежность" name="he[ethical]"
                                          value="{{$arResult['AUTH_DATA']['data']['he']['ethical'] or ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Она</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Возраст" name="she[age]"
                                           value="{{$arResult['AUTH_DATA']['data']['she']['age'] or ''}}">
                                    <input type="text" class="form-control" placeholder="Рост" name="she[growth]"
                                           value="{{$arResult['AUTH_DATA']['data']['she']['growth'] or ''}}">
                                    <input type="text" class="form-control" placeholder="Телосложение" name="she[body_type]"
                                           value="{{$arResult['AUTH_DATA']['data']['she']['body_type'] or ''}}">
                                    <input type="text" class="form-control" placeholder="Этичная принадлежность" name="she[ethical]"
                                           value="{{$arResult['AUTH_DATA']['data']['she']['ethical'] or ''}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Продолжить
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="step" value="data">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
