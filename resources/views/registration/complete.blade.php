@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Регистрация</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="error">{!! session()->get('error') !!}</div>
                            <h2>Введите e-mail и придумайте пароль</h2>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Логин" name="login"
                                       value="{{$arResult['AUTH_DATA']['register']['login'] or ''}}">


                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Город" name="city"
                                       value="{{$arResult['AUTH_DATA']['register']['city'] or ''}}">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="E-mail" name="email"
                                       value="{{$arResult['AUTH_DATA']['register']['email'] or ''}}">

                            </div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" placeholder="Пароль" name="password"
                                       value="">
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Продолжить
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="step" value="complete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
