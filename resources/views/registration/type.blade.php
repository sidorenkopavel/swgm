@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Регистрация</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="error">{!! session()->get('error') !!}</div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Тип</label>
                                <div class="col-md-6">
                                    @foreach($arResult['TYPES'] as $type)
                                        <label>
                                            <input type="radio"
                                                   name="type"
                                                   value="{{$type}}"
                                                   @if(isset($arResult['AUTH_DATA']['type']))
                                                   @if($arResult['AUTH_DATA']['type'] == $type)
                                                   checked
                                                   @endif
                                                   @endif
                                                   required
                                                   autofocus>
                                            {{$type}}
                                        </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Продолжить
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="step" value="type">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
