<?php Artisan::call('view:clear');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/register','Registration\RegisterController')->name('register');
Route::post('/register','Registration\RegisterController@save');

Route::get('/register/type','Registration\RegisterController@type')->name('register_type');
Route::get('/register/data','Registration\RegisterController@data')->name('register_data');
Route::get('/register/complete','Registration\RegisterController@complete')->name('register_complete');


$this->group(['middleware' => 'auth'], function () {
    Route::get('/personal','Personal\PersonalController')->name('personal');
    Route::post('/personal','Personal\PersonalController@update');
    Route::post('/personal-photos','Personal\PersonalController@photos')->name('personal_photos');
    Route::get('/personal/vacation/remove/{id}','Personal\PersonalController@removeVacation')->name('remove_vacation');
    Route::post('/personal-photos/remove','Personal\PersonalController@removePhoto')->name('remove_photos');
});

