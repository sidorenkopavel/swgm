<?php


namespace App\Http\Controllers\Registration;

use Session;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'login' => 'required|string|max:255|unique:users',
            'city' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function checkStepData($step)
    {
        $result = false;
        $data = Session::get('auth');
        if ($step == 'type') {
            if (!isset($data['who'])) {
                $result = redirect(route('register'))->with('error','Выберите кто вы');
            } elseif (empty($data['who'])) {
                $result =  redirect(route('register'))->with('error','Выберите кто вы');
            }
        }

        if ($step == 'data') {
            if (!isset($data['type'])) {
                $result =  redirect(route('register_type'))->with('error','Выберите тип');
            } elseif (empty($data['type'])) {
                $result =  redirect(route('register_type'))->with('error','Выберите тип');
            }
        }

        if ($step == 'complete') {
            if (!isset($data['data'])) {
                $result =  redirect(route('register_data'))->with('error','Заполните данные');
            } elseif (empty($data['data'])) {
                $result =  redirect(route('register_data'))->with('error','Заполните данные');
            }
        }

        return $result;
    }

    public function registerData()
    {

        $data['AUTH_DATA'] = Session::get('auth');
        $data['WHO'] = [
            'Пара',
            'Девушка',
            'Парень',
            'Транс'
        ];
        if (isset($data['AUTH_DATA']['who'])) {
            if ($data['AUTH_DATA']['who'] == 'Пара') {
                $data['TYPES'] = [
                    'Пара гетер',
                    'Пара Би женщина',
                    'Пара мужчина-мужчина',
                    'Пара Би мужчина',
                    'Би пара',
                    'Пара женщина-женщина',
                ];
            } else {
                $data['TYPES'] = [
                    'Гетеро',
                    'Гомо',
                    'Би',
                ];
            }
        }

        return $data;
    }

    public function __invoke()
    {
        $arResult = $this->registerData();
        return view('registration.who', compact('arResult'));
    }

    public function type()
    {
        $check = $this->checkStepData('type');
        if($check) return $check;

        $arResult = $this->registerData();
        return view('registration.type', compact('arResult'));
    }

    public function data()
    {
        $check = $this->checkStepData('data');
        if($check) return $check;

        $arResult = $this->registerData();
        if (isset($arResult['AUTH_DATA']['who'])) {
            if ($arResult['AUTH_DATA']['who'] == 'Пара') {
                return view('registration.data_couple', compact('arResult'));
            } else {
                return view('registration.data', compact('arResult'));
            }
        } else {
            return redirect(route('register'));
        }
    }

    public function complete()
    {
        $check = $this->checkStepData('complete');
        if($check) return $check;

        $arResult = $this->registerData();
        return view('registration.complete', compact('arResult'));
    }

    public function save(Request $request)
    {
        $data = $request->all();
        switch ($data['step']) {
            case 'who':
                $this->saveStepData(['who' => $data['who']]);
                return redirect(route('register_type'));
                break;
            case 'type':
                $this->saveStepData(['type' => $data['type']]);
                return redirect(route('register_data'));
                break;
            case 'data':
                $this->saveStepData(['data' => $data]);
                return redirect(route('register_complete'));
                break;
            case 'complete':
                $this->saveStepData(['register' => $data]);
                $savedData = Session::get('auth');
                unset($savedData['register']);
                $data['params'] = $savedData;
                $password = bcrypt($data['password']);
                $userData =  [
                    'email' => $data['email'],
                    'login' => $data['login'],
                    'city' => $data['city'],
                    'params' => json_encode($data['params']),
                    'password' => $password,
                    'password_confirmation' => $password,
                ];
                $res = $this->validator($userData);
                $errors = $res->errors()->all();
                if(empty($errors)){
                    User::create($userData);
                    Session::put('auth', []);
                    return redirect(route('login'));
                }
                else{
                    return redirect()->back()->with('error',implode('<br>',$errors));
                }
                break;
        }

    }

    public function saveStepData($data)
    {
        unset($data['_token'],$data['step']);
        $authData = Session::get('auth');
        foreach ($data as $key => $val) {
            $authData[$key] = $val;
        }
        Session::put('auth', $authData);
    }
}