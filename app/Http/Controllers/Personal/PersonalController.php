<?php


namespace App\Http\Controllers\Personal;

use App\Photos;
use App\User;
use App\Vacations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class PersonalController
{
    public function __invoke()
    {
        $arResult = Auth::user();
        $arResult['params'] = json_decode($arResult['params'], true);
        $arResult['interestings'] = self::getInteresting();
        $arResult['vacations'] = self::getUserVacations();


        $resultPhoto = $this->getPhotos(NULL);
        if (!empty($resultPhoto)) {
            $arResult['ITEM_IMAGE'] = [];
            foreach ($resultPhoto as &$item) {
                $url = str_replace('//', '/', $item['url']);
                $item['url'] = \URL::to('/') . '/' . $url;
                $item['size'] = filesize($url);
                $item['name'] = basename($url);
                $resultUrl[$item['name']] = $item['id'];


            }
            unset($item);
            $arResult['files'] = $resultPhoto;
            $arResult['ITEM_IMAGE'] = $resultUrl;

        }

        //   dd( $arResult['files']);

        return view('personal.personal', compact('arResult'));
    }

    public function getPhotos($parent = '')
    {
        return Photos::where('user', Auth::id())->where('parent', $parent)->get()->toArray();
    }

    public function getPopular()
    {

    }

    public function getMessages()
    {

    }

    public function getUserGallery()
    {

    }

    public function getUserVacations()
    {
        $arItems = Vacations::where('user', Auth::id())->get()->toArray();
        foreach ($arItems as &$arItem) {
            $arItem['params'] = json_decode($arItem['params'], true);
        }
        return $arItems;
    }

    public function getNotifications()
    {

    }

    public function getSympathy()
    {

    }

    public function getInteresting()
    {
        return [
            'Свинг',
            'Анал',
            'Видеочат',
            'Фото и видео',
            'Вирт',
            'Минет',
            'Эротика',
            'Массаж',
            'МЖМ',
            'БДСМ',
            'Кунилингус',
            'Групповой секс',
            'ЖМЖ',
            'Порно',
            'Секс втроем',
            'Грубый секс',
            'Реал',
            'Инцест',
            'Подглядывание',
            'Доминация',
            'Доминация',
        ];
    }

    public function createVacation($data)
    {
        if ($data['id'] == 'new') {
            $id = '';
        } else {
            $id = $data['id'];
        }
        unset($data['id']);
        Vacations::updateOrCreate(['id' => $id], $data);
    }

    public function update(Request $request)
    {

        $currentUserData = Auth::user();
        $updatedData['params'] = json_decode($currentUserData['params'], true);

        $data = $request->all();
        if (!empty($data['personal'])) {
            $updatedData['params']['data'] = array_merge($updatedData['params']['data'], $data['personal']);
        }

        if (!empty($data['vacations'])) {
            foreach ($data['vacations'] as $key => $arVacation) {
                self::createVacation(['id' => $key, 'params' => json_encode($arVacation), 'user' => Auth::id()]);
            }
        }

        $updatedData['params'] = json_encode($updatedData['params']);
        if (!empty($updatedData)) {
            User::find(Auth::id())->update($updatedData);
        }

        return redirect()->back()->with('success', 'Данные успешно обновлены');
    }

    public function removeVacation($intID)
    {
        Vacations::find($intID)->delete();
        return redirect()->back()->with('success', 'Отпуск удален');
    }

    public function photos(Request $request)
    {

        if ($request->hasFile('file')) {

            // Upload path
            $destinationPath = 'public/files/';

            // Create directory if not exists
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }

            // Get file extension
            $extension = $request->file('file')->getClientOriginalExtension();

            // Valid extensions
            $validextensions = array("jpeg", "jpg", "png", "pdf");

            // Check extension
            if (in_array(strtolower($extension), $validextensions)) {

                // Rename file
                $fileName = time() . $request->file('file')->getClientOriginalName();
                // Uploading file to given path
                $request->file('file')->move($destinationPath, $fileName);
                $data = [
                    'url' => $destinationPath . '/' . $fileName,
                    'user' => Auth::id()
                ];
                $res = Photos::updateOrCreate($data, $data);
                return Response::json(['id' => $res->id, 'result' => 'success'], 200);
            } else {
                return Response::json('error', 400);
            }
        }
        return Response::json('error', 400);
    }

    public function removePhoto(Request $request)
    {
        $result = Photos::where('id', $request->input('id'))->where('user', Auth::id())->first()->toArray();
        unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $result['url']);
        $res = Photos::where('id', $request->input('id'))->where('user', Auth::id())->delete();
        echo json_encode($res);
    }
}