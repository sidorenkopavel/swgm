<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacations extends Model
{
    protected $fillable = [
        'updated_at', 'created_at','params','user'
    ];
    protected $casts = [
        'params' => 'array'
    ];
}
